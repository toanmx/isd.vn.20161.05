package bookStore;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import account.UserInterfaceUI;
import common.Session;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
/** 
 * Lốp giao diện cho việc đăng ký mượn sách
 * @author Thanh
 *
 */
public class RegisterNewBookCopy extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JLabel status;


	/**
	 * Create the frame.
	 */
	public RegisterNewBookCopy(String bookID) {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblBookNumber = new JLabel("Book Number:");
		lblBookNumber.setBounds(72, 31, 78, 14);
		contentPane.add(lblBookNumber);
		
		JLabel lblPrice = new JLabel("Price:");
		lblPrice.setBounds(72, 84, 78, 14);
		contentPane.add(lblPrice);
		
		JLabel lblStatus = new JLabel("Status:");
		lblStatus.setBounds(72, 131, 78, 14);
		contentPane.add(lblStatus);
		
		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setBounds(160, 128, 44, 20);
		comboBox_1.addItem("available");
		comboBox_1.addItem("unavailable");
		contentPane.add(comboBox_1);
		
		textField = new JTextField();
		textField.setBounds(160, 81, 130, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblNewLabel = new JLabel(bookID);
		lblNewLabel.setBounds(160, 31, 130, 14);
		contentPane.add(lblNewLabel);
		
		JButton btnSubmit = new JButton("Submit");
		btnSubmit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try{
				String textPrice = textField.getText();
				if(textPrice.equals(""))
				{
					status.setText("*Can nhap gia!");
				}
				else
				{
				int price = Integer.parseInt(textField.getText());
				System.out.println(price);
				Books book = new Books();
				book.setBookFromID(bookID);
				BookCopy newCopy = new BookCopy(bookID,price,comboBox_1.getSelectedItem().toString());
				if(newCopy.createCopy(book, price,comboBox_1.getSelectedItem().toString()))
					{
					JOptionPane.showMessageDialog(null, "Them thanh cong!");
					dispose();
					}
				else{
					JOptionPane.showMessageDialog(null, "Them that bai!");
				}
				}
				}
				catch(NumberFormatException nfe){
					status.setText("*Gia khong hop le!");
				}
			}
		});
		btnSubmit.setBounds(160, 188, 89, 23);
		contentPane.add(btnSubmit);
		
		status = new JLabel("");
		status.setBounds(158, 222, 132, 14);
		contentPane.add(status);
	}
}
