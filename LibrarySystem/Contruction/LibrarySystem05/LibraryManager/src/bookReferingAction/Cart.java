package bookReferingAction;

import java.awt.HeadlessException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.JOptionPane;

import bookStore.BookCopy;
import bookStore.Books;
import common.DataAccessHelper;
import common.Session;
/**
 * Lớp trừu tượng hóa một giỏ sách gồm những đầu sách người mượn có ý định đăng ký mượn
 * @author Thanh
 *
 */
public class Cart extends DataAccessHelper {

	private static int count;
	private static ArrayList<Books> bookArray;
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public ArrayList<Books> getBookArray() {
		return bookArray;
	}
	public void setBookArray(ArrayList<Books> bookArray) {
		this.bookArray = bookArray;
	}
	/**
	 * Phương thức khởi tạo một giỏ sách rỗng
	 */
	public Cart()
	{
		this.bookArray = new ArrayList<Books>();
		this.count = bookArray.size();
	}
	
	/**
	 * Thêm các đối tượng lớp Book trong mảng bookList vào giỏ sách
	 * @param bookList 
	 * @return thêm thành công hoặc thất bại
	 */
	public boolean addBookToCart(ArrayList<Books> bookList)
	{
		if (this.count+bookList.size()>5)
		{
			JOptionPane.showMessageDialog(null, "Khong the muon nhieu hon 5 quyen sach mot luc!");
			return false;
		}
			
		for(Books ac: bookList)
		{
			if(!isCartContains(ac))
			{
			this.bookArray.add(ac);
			this.count++;
			}
		}
		return true;
	}
	
	/**
	 * Xóa một đối tượng lớp Books ra khỏi giỏ sách
	 * @param book
	 * @return xóa thành công hoặc thất bại
	 */
	public boolean deleteFromCart(Books book)
	{
		for(Books ac: this.bookArray)
		{
			if(ac.isEquals(book))
			{
			this.bookArray.remove(ac);
			this.count--;
			return true;
			}
			System.out.println(ac.getBookID());
		}
		return false;
	}
	
	/**
	 * Phương thức kiểm tra xem trong giỏ sách đã có sách book chưa
	 * @param book
	 * @return true nếu có và false nếu không
	 */
	public boolean isCartContains(Books book)
	{
		for(Books b:this.bookArray)
		{
			if(b.getBookID().equals(book.getBookID())) return true;
		}
		return false;
	}
	/**
	 * xác nhận yêu cầu mượn sách trong giỏ hàng
	 * @return thành công hoặc thất bại
	 * @throws SQLException
	 * @throws ParseException
	 */
	public boolean submitCart() throws SQLException, ParseException
	{
		String userID = Session.getUserID();
		String cardID = Session.getCardID();
		String sql0 = "select * from borrowing_general as b_g inner join borrowing_detail as b_d on b_g.borrowing_id = b_d.borrowing_id where b_g.card_id = '"+cardID+"' and b_g.expected_return_date < DATE_SUB(CURRENT_TIMESTAMP, INTERVAL 1 DAY) and b_d.status = 'borrowing'";
		String sql = "SELECT * FROM `borrowing_general` as g inner join borrowing_detail as d on d.borrowing_id = g.borrowing_id inner join card on card.card_id = g.card_id inner join user on user.user_id = card.user_id WHERE user.user_id = "
		+ Session.getUserID()
		+" and d.status in ('pending','borrowing')";
		String sql2 = "Select * from card where card_id = '"+Session.getCardID()+"'";
		System.out.println(sql0+"; "+sql);
		getConnection();
		ResultSet rs0 = getStmt().executeQuery(sql0);
		if(rs0.next())
		{
			JOptionPane.showMessageDialog(null, "Vui long tra lai sach muon da qua han truoc khi muon them!");
			return false;
		}
		else
		{
		ResultSet rs = getStmt().executeQuery(sql);
		rs.last();
		int numOfCopy = rs.getRow();
		if(numOfCopy+this.getCount()>5)
		{
			JOptionPane.showMessageDialog(null, "Khong the muon nhieu hon 5 quyen mot luc", "Loi!", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		else
		{
		rs = getStmt().executeQuery(sql2);
		rs.next();
		SimpleDateFormat sdfm = new SimpleDateFormat("yyyy-MM-dd");
		Date today = new Date();
		if(!today.before(sdfm.parse(rs.getString("expired_date"))))
				{
					JOptionPane.showMessageDialog(null, "The muon da het han!");
					return false;
				}
		
		else
		{	
			BorrowingGeneral borGen = new BorrowingGeneral(Session.getCardID(), new Date());
			System.out.println(Session.getCardID());
			borGen.addToDB();
			ArrayList<Books> unAvailBook = new ArrayList<Books>();
			boolean check = true;
			for(Books b:this.getBookArray())
			{
				BookCopy bookCopy = new BookCopy();
				String availableCopyID = bookCopy.getAvailableCopy(b.getBookID());
				if(availableCopyID.equals(""))
				{
					unAvailBook.add(b);
				}
				else
				{
					bookCopy.setCopyFromID(availableCopyID);
					BorrowingDetail newDetail = new BorrowingDetail(borGen.getBorrowingID(), availableCopyID);
					check = check&newDetail.addToDB();
					
				}
			}
			if(check)
			{
				JOptionPane.showMessageDialog(null, "Dang ky thanh cong!");
				
			}
			else JOptionPane.showMessageDialog(null, "Dang ky khong thanh cong!");
			
			if(!unAvailBook.isEmpty())
			{
				String mes = "Cac dau sach sau hien khong ranh: ";
				for(Books b:unAvailBook)
				{
					mes = mes+b.getTitle()+", ";
				}
				mes=mes+"vui long quay lai sau!";
				JOptionPane.showMessageDialog(null, mes);
			}
			return true;
		}
		
	}
		}}
}
