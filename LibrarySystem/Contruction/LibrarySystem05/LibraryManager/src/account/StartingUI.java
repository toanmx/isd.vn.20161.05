package account;

import java.awt.BorderLayout;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import bookStore.BrowseCatalogUI;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


/**
* 
* Class giao diện khởi đầu khi chạy chương trình
*
* @author  Hoang Hai Thanh
* @since   2016-10-20
*/

public class StartingUI extends JFrame {

	private JPanel contentPane;

	/**
	 * Phương thức main cho cả chương trình
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					StartingUI frame = new StartingUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Phương thức khởi tạo giao diện
	 */
	public StartingUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 400);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnNewButton = new JButton("Browse Book Catalog");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				BrowseCatalogUI brwCata = new BrowseCatalogUI();
				brwCata.setVisible(true);
				dispose();
			}
		});
		btnNewButton.setBounds(130, 45, 158, 42);
		contentPane.add(btnNewButton);
		
		JButton btnBrowseBook = new JButton("Login");
		btnBrowseBook.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				LoginUI newLoginUI = new LoginUI();
				newLoginUI.setVisible(true);
				dispose();
			}
		});
		btnBrowseBook.setBounds(130, 223, 158, 42);
		contentPane.add(btnBrowseBook);
		
		JButton btnRegisterAccount = new JButton("Register Account");
		btnRegisterAccount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				RegisterAccountUI regAcc = new RegisterAccountUI();
				regAcc.setVisible(true);
				dispose();
			}
		});
		btnRegisterAccount.setBounds(130, 137, 158, 42);
		contentPane.add(btnRegisterAccount);
	}
}
