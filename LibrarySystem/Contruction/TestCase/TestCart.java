package TestCase;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.*;

import bookReferingAction.Cart;
import bookStore.Books;

public class TestCart {
private Cart cart = new Cart();
ArrayList<Books> bookArr1 = new ArrayList<Books>();
ArrayList<Books> bookArr2 = new ArrayList<Books>();
Books book1 = new Books();
Books book2 = new Books();
Books book3 = new Books();
Books book4 = new Books();
Books book5 = new Books();
Books book6 = new Books();
Books book7 = new Books();
Books book8 = new Books();
Books book9 = new Books();
Books book10 = new Books();

@Test
public void testCase1AddToCart()
{
	book1.setBookFromID("CT0001");
	book2.setBookFromID("CT0002");
	book3.setBookFromID("CT0003");
	book4.setBookFromID("CT0004");
	book5.setBookFromID("GT0001");
	book6.setBookFromID("KH0001");
	book7.setBookFromID("PN0001");
	book8.setBookFromID("PN0002");
	book9.setBookFromID("SK0001");
	book10.setBookFromID("TC0002");
	bookArr1.add(book1);
	bookArr1.add(book2);
	bookArr1.add(book3);
	bookArr1.add(book4);
	bookArr1.add(book5);
	bookArr2.add(book1);
	bookArr2.add(book2);
	bookArr2.add(book3);
	bookArr2.add(book4);
	bookArr2.add(book5);
	//bookArr1 du dieu kien de add
	
	assertTrue(cart.addBookToCart(bookArr1));
	assertTrue(cart.isCartContains(book1));
	assertTrue(cart.isCartContains(book2));
	assertTrue(cart.isCartContains(book3));
	assertTrue(cart.isCartContains(book4));
	assertTrue(cart.isCartContains(book5));
	assertFalse(cart.isCartContains(book6));
	//bookArr1 giong bookArr2
	assertEquals(bookArr2,cart.getBookArray());
}

@Test
public void testCase2AddToCart()
{
	book1.setBookFromID("CT0001");
	book2.setBookFromID("CT0002");
	book3.setBookFromID("CT0003");
	book4.setBookFromID("CT0004");
	book5.setBookFromID("GT0001");
	book6.setBookFromID("KH0001");
	book7.setBookFromID("PN0001");
	book8.setBookFromID("PN0002");
	book9.setBookFromID("SK0001");
	book10.setBookFromID("TC0002");
	bookArr1.add(book1);
	bookArr1.add(book2);
	bookArr1.add(book3);
	bookArr1.add(book4);
	bookArr1.add(book5);
	bookArr1.add(book6);
	bookArr2.add(book1);
	bookArr2.add(book2);
	bookArr2.add(book3);
	bookArr2.add(book4);
	bookArr2.add(book5);
	
	//bookArr1 chua nhieu hon 5 phan tu
	assertFalse(cart.addBookToCart(bookArr1));
	//Them false nen trong cart van chang co gi!
	assertEquals(0, cart.getCount());
}

@Test
public void testCase3AddToCart()
{
	book1.setBookFromID("CT0001");
	book2.setBookFromID("CT0002");
	book3.setBookFromID("CT0003");
	book4.setBookFromID("CT0004");
	book5.setBookFromID("GT0001");
	book6.setBookFromID("KH0001");
	book7.setBookFromID("PN0001");
	book8.setBookFromID("PN0002");
	book9.setBookFromID("SK0001");
	book10.setBookFromID("TC0002");
	bookArr1.add(book1);
	bookArr1.add(book2);
	bookArr1.add(book3);
	bookArr1.add(book4);
	bookArr1.add(book5);
	bookArr2.add(book1);
	bookArr2.add(book2);
	bookArr2.add(book3);
	bookArr2.add(book4);
	
	//bookArr1 du dieu kien de add
	assertTrue(cart.addBookToCart(bookArr1));
	assertEquals(5, cart.getCount());
	//xoa thanh cong book 5 khoi danh sach book trong cart
	assertTrue(cart.deleteFromCart(book5));
	assertEquals(4, cart.getCount());
	//sau khi xoa book5 thi danh sach book trong cart giong voiw bookArr2
	assertEquals(bookArr2,cart.getBookArray());
}
}
