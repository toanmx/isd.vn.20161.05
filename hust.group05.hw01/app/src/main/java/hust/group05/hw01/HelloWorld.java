package hust.group05.hw01;

import android.content.DialogInterface;
import android.content.Intent;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.lang.*;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;

public class HelloWorld extends AppCompatActivity {

    private EditText etName,etYearOfBirth;
    private int yearOfBirth;
    Calendar calendar = Calendar.getInstance();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hello_world);

        //Khởi tạo edittext view

        etName = (EditText) findViewById(R.id.etName);
        etYearOfBirth = (EditText) findViewById(R.id.etYearOfBirth);


    }

    //Xử lý sự kiện click button "Next"
    //Khi click vào button "Next" thì ứng dung chuyển sang màn hình First Number App

    public void OnClickNext(View view){
        startActivity(new Intent(HelloWorld.this,FirstNumberApp.class));
    }
    //Xử lý sự kiện click button "Ok"
    public void OnClickOk(View view){
        yearOfBirth = java.lang.Integer.parseInt(etYearOfBirth.getText().toString());

        if (yearOfBirth>  calendar.get(Calendar.YEAR)){
            Toast.makeText(HelloWorld.this,"Wrong year of birth.Please try again!",Toast.LENGTH_SHORT).show();
            return;
        }
        else if (yearOfBirth <=1910){
            Toast.makeText(HelloWorld.this,"You are too old to type this text.Please rest in peace!",Toast.LENGTH_LONG).show();
            return;
        }
        showHelloDialog();
    }


    //Phương thức tạo dialog dùng để show lên tên và tuổi của ng nhập.
    private void showHelloDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(HelloWorld.this);

        //Set title cho dialog

        builder.setTitle("Hello " + etName.getText().toString()+"!!");
        builder.setMessage("Your age is " + (calendar.get(Calendar.YEAR)-yearOfBirth));

        String positiveText = "Ok";

        builder.setPositiveButton(positiveText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        AlertDialog dialog = builder.create();
        // display dialog

        dialog.show();
    }
}
