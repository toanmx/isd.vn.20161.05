package hust.group05.hw01;

/**
 * Created by Administrator on 9/3/2016.
 */
import static java.lang.Math.pow;
public class Integer {
    private int value;

    public Integer(int value) {
        this.value = value;
    }

    public boolean checkPrime()
    {
        if (this.value==1 || this.value<=0) return false;
        else if (this.value==2) return true;
        else if (this.value==3||this.value==5) return true;
        else if (this.value > 2 && this.value%2 ==0) return false;
        else
        {
            for (int i = 3;i<=(int) Math.sqrt(value);i+=2)
            {
                if (this.value%i==0) return false;
            }
            return true;
        }
    }

    public boolean checkSquare()
    {
        if(this.value==1||this.value==0) return true;
        else if(this.value<0) return false;
        else {
            for(int i = 1;pow(i,2)<=this.value;i++)
            {
                if (this.value == pow(i,2)) return true;
            }
            return false;
        }
    }

    public boolean checkPerfect()
    {
        int tmp = this.value;
        if(this.value==1||this.value<=0) return false;
        for(int i = 1;i<=this.value/2;i++)
        {
            if(this.value%i==0) tmp-=i;
        }
        if(tmp==0) return true;
        else return false;
    }

    public boolean checkComposite()
    {
        if (this.value==1 || this.value<=0) return false;
        else if (this.value==2) return false;
        else if (this.value==3||this.value==5) return false;
        else {
            if (this.value > 2 && this.value % 2 == 0) return true;
            else {
                for (int i = 3; i <= this.value / 2; i += 2) {
                    if (this.value % i == 0) return true;
                }
                return false;
            }
        }
    }
}
