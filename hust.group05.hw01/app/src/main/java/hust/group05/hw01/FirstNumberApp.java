package hust.group05.hw01;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import java.lang.*;

/**
 * Created by Admin on 9/3/2016.
 */
public class FirstNumberApp extends AppCompatActivity{

    private CheckBox cbPrimaryNumber,cbCompositeNumber,cbSquareNumber,cbPerfectNumber;

    private EditText etIntegerNumber;
    private Integer num;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_number_app);

        inititalCheckBox();

        etIntegerNumber = (EditText) findViewById(R.id.etEnterAIntegerNumber);


    }
    private void inititalCheckBox(){
        cbPrimaryNumber = (CheckBox) findViewById(R.id.cbPrimeNumber);
        cbCompositeNumber = (CheckBox) findViewById(R.id.cbCompositeNumber);
        cbPerfectNumber = (CheckBox) findViewById(R.id.cbPerfectNumber);
        cbSquareNumber = (CheckBox) findViewById(R.id.cbSquareNumber);
    }
    public void OnClickBack(View view){
        finish();
    }

    public void OnClickCheck(View view){
        int i = java.lang.Integer.parseInt(etIntegerNumber.getText().toString());
        if (i<0){
            Toast.makeText(FirstNumberApp.this,"Your number must be greater than 0. Please try again!",Toast.LENGTH_SHORT).show();
            cbPrimaryNumber.setChecked(false);
            cbCompositeNumber.setChecked(false);
            cbPerfectNumber.setChecked(false);
            cbSquareNumber.setChecked(false);
            return;
        }
        else if (i==1){
            cbPrimaryNumber.setChecked(false);
            cbCompositeNumber.setChecked(false);
            cbPerfectNumber.setChecked(false);
            cbSquareNumber.setChecked(true);
            return;
        }
        num = new Integer(i);
        cbPrimaryNumber.setChecked(num.checkPrime());
        cbCompositeNumber.setChecked(!num.checkPrime());
        cbPerfectNumber.setChecked(num.checkPerfect());
        cbSquareNumber.setChecked(num.checkSquare());

    }

}
