package bookStore;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import common.DataAccessHelper;
/**
 * Lớp trừu tượng hóa thông tin của một nhà phát hành
 * @author Thanh
 *
 */
public class Publisher extends DataAccessHelper{
	private int publisherID;
	private String publisherName;
/**
 * Phương thức khởi tạo nph rỗng
 */
	public Publisher() {
		publisherName = "";
	}
/**
 * Phương thức khởi tạo đối tượng nph theo thông tin cho trước
 * @param publisherID
 * @param publisherName
 */
	public Publisher(int publisherID, String publisherName) {
		super();
		this.publisherID = publisherID;
		this.publisherName = publisherName;
	}
	/**
	 * Phương thức đặt thuộc tính của đối tượng hiện tại theo thông tin của nhà phát hành có id cho trước
	 */
	public boolean setPublisherFromID(int publisherID)
	{
		String sql = "SELECT * FROM book_publisher WHERE publisher_id = '"+publisherID+"'";
		try {
			getConnection();
			ResultSet rs = getStmt().executeQuery(sql);
			
			if(!rs.next())
			{
				JOptionPane.showMessageDialog(null, "Khong ton tai Category co ID la: "+publisherID);
				closeConnection();
				return false;
			}
			else
			{
				this.publisherID = Integer.parseInt(rs.getString("publisher_id"));
				this.publisherName = rs.getString("publisher_name");
				closeConnection();
				return true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			closeConnection();
			e.printStackTrace();
			return false;
		}
		
	}
	
	public int getPublisherID() {
		return publisherID;
	}

	public String getPublisherName() {
		return publisherName;
	}

	public void setPublisherID(int publisherID) {
		this.publisherID = publisherID;
	}

	public void setPublisherName(String publisherName) {
		this.publisherName = publisherName;
	}
/**
 * Phương thức tạo mảng tất cả các nhà phát hành trong csdl
 * @return mảng các đối tượng nhà phát hành
 * @throws SQLException
 */
	public ArrayList<Publisher> getListBookPublisher() throws SQLException {
		ArrayList<Publisher> list = new ArrayList<Publisher>();
		getConnection();
		String sql = "SELECT publisher_id, publisher_name FROM book_publisher";
		ResultSet rs = getStmt().executeQuery(sql);
		while (rs.next()) {
			list.add(new Publisher(rs.getInt("publisher_id"), rs.getString("publisher_name")));
		}
		return list;
	}
/**
 * Phương thức trả về tên nhà phát hành
 */
	public String toString() {
		if (publisherName.equals(""))
			return "Tat ca";
		return publisherName;
	}
}
