package bookStore;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import common.DataAccessHelper;

/**
 * Lớp trừu tượng hóa một bản sao sách
 * @author Thanh
 *
 */
	public class BookCopy extends DataAccessHelper {
		private String bookCopyID;
		private String bookID;
		private String status;
		private int price, sequenceNumber;

		public static final int STATUS_AVAILABLE = 1;
		public static final int STATUS_NOT_AVAILABLE = 0;
/**
 * Phương thức khởi tạo một bản sao rỗng
 */
		public BookCopy(){
			
		}
		/**
		 * Phương thức khởi tạo một bản sao từ các thông tin cho trước
		 * @param bookID
		 * @param price
		 * @param status
		 */
		public BookCopy(String bookID, int price, String status) {
			super();
			this.bookCopyID = bookID+String.format("%1$02d", sequenceNumber);
			this.bookID = bookID;
			this.sequenceNumber = sequenceNumber;
			this.price = price;
			this.status = status;
		}
		/**
		 * Phương thức set thuộc tính của đối tượng là các thông tin của bản sao có mã là copyID
		 */
		public void setCopyFromID(String copyID) throws SQLException
		{
			String sql =  "select * from book_copy where copy_id = '"+copyID+"'";
			getConnection();
			ResultSet rs = getStmt().executeQuery(sql);
			if(rs.next())
			{
				this.bookCopyID = rs.getString("copy_id");
				this.bookID=rs.getString("book_id");
				this.price = rs.getInt("price");
				this.status = rs.getString("status");
				this.sequenceNumber = rs.getInt("sequence_number");
			}
			else{
				JOptionPane.showMessageDialog(null, "Khong ton tai copy co id la "+bookID);
			}
			closeConnection();
		}

		public String getBookCopyID() {
			return bookCopyID;
		}
		
		public int getSequenceNumber()
		{
			return this.sequenceNumber;
		}
		
		public int getPrice() {
			return price;
		}

		public String getStatus() {
			return status;
		}
/**
 * Phương thức lấy về mã sách bé nhất của bản sao đang rảnh có bookId cho trước
 * @param bookID
 * @return mã id bản sao rảnh hoặc "" nếu không có bản sao nào rảnh
 * @throws SQLException
 */
		public String getAvailableCopy(String bookID) throws SQLException {
			String copyID = "";
			String sql = "SELECT copy_id FROM book_copy WHERE book_id LIKE '" + bookID + "' AND status = 'available' LIMIT 1";
			getConnection();
			ResultSet rs = getStmt().executeQuery(sql);
			if (rs.next())
				copyID = rs.getString("copy_id");
			closeConnection();
			return copyID;
		}
		/**
		 * phương thức sinh mã thứ tự cho một bản sao có mã sách cho trước 
		 * @param book
		 * @return má bản sao được sinh
		 * @throws SQLException
		 */
		private int bookCopySequenceNumberGenerator(Books book) throws SQLException {
			String bookID = book.getBookID();
			String sql = "SELECT * FROM book_copy WHERE book_ID LIKE '" + bookID + "' ORDER BY sequence_number DESC LIMIT 1";
			System.out.println(sql);
			getConnection();
			ResultSet rs = getStmt().executeQuery(sql);
			int id = 1;
			if (rs.next()) {
				
				id = Integer.parseInt(rs.getString("sequence_number"))+1;
				
			}
			return id;
		}
		/**
		 * Thêm một bản ghi trong bảng book_copy có dữ liệu cho trước
		 * @param book
		 * @param price
		 * @param status
		 * @return
		 */
		public boolean createCopy(Books book, int price, String status)
		{
			int sequenceNumber;
			try {
				sequenceNumber = bookCopySequenceNumberGenerator(book);
				String sql = "INSERT INTO book_copy(sequence_number,book_id,price,status) VALUES ("+sequenceNumber+", '"+book.getBookID()+"', "+price+", '"+status+"')";
				return updateQuery(sql);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			}
			
		}
		/**
		 * Phương thức chỉnh sửa trạng thái của một bản sao theo trạng thái cho trước
		 * @param copyID
		 * @param status
		 * @return
		 * @throws SQLException
		 */
		public boolean editStatus(int copyID, String status) throws SQLException {
//			if (status != STATUS_AVAILABLE && status != STATUS_NOT_AVAILABLE)
//				return false;
//			else {
				String sql = "UPDATE book_copy SET status = '" + status + "' WHERE copy_id = " + copyID + ")";
				return updateQuery(sql);
			
		}

	}

