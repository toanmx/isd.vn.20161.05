package bookStore;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import common.DataAccessHelper;
/**
 * Lớp trừu tượng hóa sách, có thuộc tính là các thuộc tính của sách được sử dụng bởi hệ thống, các phương thức tương tác với csdl
 * @author Thanh
 *
 */
public class Books extends DataAccessHelper {
	private String bookID;
	private String title, author, isbn;
	private Publisher publisher;
	private Category category;
	public Books() {

	}
	/**
	 * đặt các thuộc tính cho đối tượng từ thông tin từ sách có mã sách là bookID
	 * @param bookID
	 * @return thành công hoặc thất bại
	 */
	public boolean setBookFromID(String bookID)
	{
		String sql = "SELECT * FROM book WHERE book_id = '"+bookID+"'";
		try {
			getConnection();
			ResultSet rs = getStmt().executeQuery(sql);
			
			if(!rs.next())
			{
				JOptionPane.showMessageDialog(null, "Khong ton tai Sach co ID la: "+bookID);
				return false;
			}
			else
			{
				this.bookID = rs.getString("book_id");
				this.author = rs.getString("author");
				this.title = rs.getString("title");
				this.isbn = rs.getString("isbn");
				String publisherID = rs.getString("publisher_id");
				String categoryID = rs.getString("category_id");
				closeConnection();
				Publisher p = new Publisher();
				p.setPublisherFromID(Integer.parseInt(publisherID));
				this.publisher = p;
				Category c = new Category();
				c.setCategoryFromID(categoryID);
				this.category = c;
				
				return true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			closeConnection();
			e.printStackTrace();
			return false;
		}
		
		
	}
	/**
	 * Khởi tạo một đối tượng sách từ các thông tin cho trước
	 * @param bookID
	 * @param title
	 * @param author
	 * @param isbn
	 * @param publisher
	 * @param category
	 */
	public Books(String bookID, String title, String author, String isbn, Publisher publisher, Category category) {
		super();
		this.bookID = bookID;
		this.title = title;
		this.author = author;
		this.isbn = isbn;
		this.publisher = publisher;
		this.category = category;
	}

	public String getBookID() {
		return bookID;
	}

	public String getTitle() {
		return title;
	}

	public String getAuthor() {
		return author;
	}

	public String getIsbn() {
		return isbn;
	}

	public Publisher getPublisher() {
		return publisher;
	}

	public Category getCategory(){
		return category;
	}
	
	public void setBookID(String bookID) {
		this.bookID = bookID;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public void setPublisher(Publisher publisher) {
		this.publisher = publisher;
	}
	
	public void setCategory(Category category)
	{
		this.category = category;
	}
	/**
	 * update dữ liệu trong bản ghi có mã sách là bookID theo các thuộc tính của đối tượng hiện tại
	 * @param bookID
	 * @param title
	 * @param author
	 * @param isbn
	 * @param publisherID
	 * @param categoryID
	 * @return
	 * @throws SQLException
	 */
	public boolean update(String bookID, String title, String author, String isbn, int publisherID, String categoryID)
			throws SQLException {
		String sql = "UPDATE book SET title = " + title + ", author = " + author + ", isbn = " + isbn
				+ ", publisher_id = " + publisherID + ", category_id = " + categoryID + " WHERE book_id LIKE " + bookID;
		return updateQuery(sql);
	}
	
	
	/**
	 * Xóa bản ghi có book_Id là bookID khỏi bảng Book
	 * @return
	 * @throws SQLException
	 */
	public boolean delete() throws SQLException {
		String sql = "DELETE FROM book WHERE book_id = " + bookID;
		return updateQuery(sql);
	}
/**
 * Phương thức  sinh mã sách từ một thể loại
 * @param categoryID
 * @return
 * @throws SQLException
 */
	private String bookIdGenerator(String categoryID) throws SQLException {
		String bookID = "'" + categoryID + "%'";
		String sql = "SELECT book_id FROM book WHERE book_ID LIKE " + bookID + "ORDER BY book_ID DESC LIMIT 1";
		getConnection();
		ResultSet rs = getStmt().executeQuery(sql);
		String id = "0001";
		if (rs.next()) {
			String result = rs.getString("book_id");
			id = result.substring(2);
		}
		String code = String.format("%1$04d", Integer.parseInt(id)+1);
		return  categoryID + code;
	}

	/**
	 * Ghi mới một bản ghi vào bảng book với các dữ liệu cho trước
	 * @param categoryID
	 * @param title
	 * @param author
	 * @param isbn
	 * @param publisherID
	 * @return thành công hoặc thất bại
	 * @throws SQLException
	 */
	public boolean create(String categoryID, String title, String author, String isbn, int publisherID)
			throws SQLException {
		this.title = title;
		this.author = author;
		this.isbn = isbn;
		Publisher p = new Publisher();
		p.setPublisherFromID(publisherID);
		this.publisher = p;
		Category c = new Category();
		c.setCategoryFromID(categoryID);
		this.category = c;

		String bookID = bookIdGenerator(categoryID);
		String sql = "INSERT INTO book (book_id, title, author, isbn, publisher_id, category_id) VALUES ('"
				+ bookID + "', '" + title + "', '" + author + "', '" + isbn + "', '" + publisherID
				+ "','" + categoryID + "')";
		this.bookID = bookID;
		
		return updateQuery(sql);
	}
/**
 * khởi tạo các book có thông tin trong Resultset và đưa vào mảng sách
 * @param rs
 * @return mảng sách
 * @throws SQLException
 */
	private ArrayList<Books> fetchResult(ResultSet rs) throws SQLException {
		ArrayList<Books> arr = new ArrayList<Books>();
		if (rs != null) {
			try {
				while (rs.next()) {
					arr.add(new Books(rs.getString("book_id"), rs.getString("title"), rs.getString("author"),
							rs.getString("isbn"),
							new Publisher(rs.getInt("publisher_id"), rs.getString("publisher_name")),new Category(rs.getString("category_id"),rs.getString("category_name"))));
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return arr;
	}
/**
 * phương thức khởi tạo tất cả cách sách trong csdl và cho vào mảng arr
 * @return arr
 * @throws SQLException
 */
	public ArrayList<Books> getListBook() throws SQLException {
		String sql = "SELECT b.book_id, b.title, b.author, b.isbn,"
				+ " p.publisher_id, p.publisher_name, c.category_id, c.category_name FROM book_category AS c INNER JOIN book AS b ON c.category_id = b.category_id INNER JOIN book_publishter AS p ON b.publisher_id = p.publisher_id";
		getConnection();
		ResultSet rs = getStmt().executeQuery(sql);
		ArrayList<Books> arr = fetchResult(rs);
		closeConnection();
		return arr;
	}
///**
// * Tạo mảng các Sách từ trường tìm kiếm isbn hoặc book_id
// * @param input
// * @param field
// * @return
// * @throws SQLException
// */
//	public ArrayList<Books> getListBook(String input, String field) throws SQLException
//	{
//		String sql;
//		if(field.equals("isbn"))
//		{
//			sql = "SELECT * FROM book WHERE book_isbn = '"+input+"'";
//		}
//		else
//			if(field.equals("book_id"))
//			{
//				sql = "SELECT * FROM book WHERE book_id = '"+input+"'";
//			}
//			else return null;
//		getConnection();
//		ResultSet rs = getStmt().executeQuery(sql);
//		ArrayList<Books> arr = fetchResult(rs);
//		closeConnection();
//		return arr;
//	}
//	
	public boolean isEquals(Books book)
	{
		return getBookID().equals(book.getBookID());
	}
	/**
	 * Tạo mảng các sách có thông tin cho trước
	 * @param title
	 * @param author
	 * @param categoryID
	 * @param publisherName
	 * @param isbn
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<Books> getListBook(String title, String author, String categoryID, String publisherName, String isbn)
			throws SQLException {
		title = "'%" + title + "%'";
		author = "'%" + author + "%'";
		String bookID = "'" + categoryID + "%'";
		publisherName = "'%" + publisherName + "%'";
		isbn = "'%" + isbn + "%'";
		String sql = "select b.book_id, b.title, b.author, b.isbn, p.publisher_id,p.publisher_name,c.category_id, c.category_name "
				+ "from book_category as c "
				+ "inner join book as b on c.category_id = b.category_id "
				+ "inner join book_publisher as p on b.publisher_id = p.publisher_id " 
				+ "where b.title like "+title+ " and b.author like "+author+" and b.book_id like "+bookID+" and p.publisher_name like "+publisherName + " and b.isbn like "+isbn; 

		getConnection();
		ResultSet rs = getStmt().executeQuery(sql);
		ArrayList<Books> arr = fetchResult(rs);
		closeConnection();
		return arr;
	}
}
