package bookStore;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Lớp điều khiên sách
 * @author Thanh
 *
 */
public class BookController {
	/**
	 * Phương thức tìm kiếm sách có các thông tin cho trước
	 * @param categoryID
	 * @param title
	 * @param author
	 * @param publisherName
	 * @param isbn
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<Books> searchBook(String categoryID, String title, String author, String publisherName, String isbn)
			throws SQLException {
		Books book = new Books();
		return book.getListBook(title, author, categoryID, publisherName,isbn);
	}
/**
 * Phương thức lấy các category có trong csdl
 * @return mảng các category
 */
	public ArrayList<Category> getAllCategory() {
		Category bc = new Category();
		try {
			return bc.getListBookCategory();
		} catch (SQLException e) {
			e.printStackTrace();
			return new ArrayList<Category>();
		}
	}
	/**
	 * phương thức lấy các nhà xuất bản có trong csdl
	 * @return mảng các nhà xuất bản
	 */
	public ArrayList<Publisher> getAllPublisher() {
		Publisher p = new Publisher();
		try {
			return p.getListBookPublisher();
		} catch (SQLException e) {
			e.printStackTrace();
			return new ArrayList<Publisher>();
		}
	}
}
