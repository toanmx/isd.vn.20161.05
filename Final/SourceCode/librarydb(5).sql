-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 29, 2016 at 03:26 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `librarydb`
--

-- --------------------------------------------------------

--
-- Table structure for table `book`
--

CREATE TABLE `book` (
  `book_id` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `author` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `isbn` bigint(10) UNSIGNED ZEROFILL NOT NULL,
  `publisher_id` int(6) UNSIGNED ZEROFILL NOT NULL,
  `category_id` varchar(2) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `book`
--

INSERT INTO `book` (`book_id`, `title`, `author`, `isbn`, `publisher_id`, `category_id`) VALUES
('CT0001', 'Viet Nam Ngay Nay', 'Hoang Hai Thanh', 9845549758, 000011, 'CT'),
('CT0002', 'title1', 'author 3', 1234567891, 000012, 'CT'),
('CT0003', 'Nhung Dieu Muon Noi', 'Hoang Hai Thanh', 8765432123, 000012, 'CT'),
('CT0004', 'Hoang Hai Thanh', 'Hoang Hai Thanh', 1234569874, 000012, 'CT'),
('GT0001', 'title gt 1', 'Nguyen Thu Hang', 7894513621, 000012, 'GT'),
('KH0001', 'title kh 1', 'author2', 0000865423, 000013, 'KH'),
('PN0001', 'title pn 1', 'author3', 0000802398, 000014, 'PN'),
('PN0002', 'title2', 'author 1', 1234569871, 000013, 'PN'),
('SK0001', 'title sk 01', 'author 4', 0006796521, 000015, 'SK'),
('TC0002', 'title2', 'author 3', 9876543219, 000020, 'TC');

-- --------------------------------------------------------

--
-- Table structure for table `book_category`
--

CREATE TABLE `book_category` (
  `category_id` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `category_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `book_category`
--

INSERT INTO `book_category` (`category_id`, `category_name`) VALUES
('CT', 'Chinh Tri'),
('GT', 'Giai Tri'),
('KH', 'Khoa Hoc'),
('PN', 'Phu Nu'),
('SK', 'Suc Khoe'),
('TC', 'Tinh Cam Lang Mang'),
('TG', 'Ton Giao'),
('VH', 'Van Hoa'),
('VN', 'Van Nghe'),
('XH', 'Xa Hoi');

-- --------------------------------------------------------

--
-- Table structure for table `book_copy`
--

CREATE TABLE `book_copy` (
  `book_id` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `sequence_number` int(2) UNSIGNED ZEROFILL NOT NULL DEFAULT '01',
  `price` int(10) NOT NULL,
  `status` enum('available','unavailable') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'available',
  `copy_id` varchar(8) AS (CONCAT(book_id,sequence_number)) PERSISTENT
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `book_copy`
--

INSERT INTO `book_copy` (`book_id`, `sequence_number`, `price`, `status`, `copy_id`) VALUES
('CT0001', 01, 90000, 'available', 'CT000101'),
('CT0001', 02, 90000, 'available', 'CT000102'),
('CT0001', 03, 80000, 'available', 'CT000103'),
('CT0002', 01, 90000, 'unavailable', 'CT000201'),
('CT0002', 02, 9000, 'available', 'CT000202'),
('CT0002', 03, 900000, 'unavailable', 'CT000203'),
('GT0001', 01, 90000, 'available', 'GT000101'),
('GT0001', 02, 90000, 'available', 'GT000102'),
('GT0001', 03, 70000, 'available', 'GT000103'),
('KH0001', 01, 80000, 'available', 'KH000101'),
('KH0001', 02, 80000, 'available', 'KH000102'),
('PN0001', 01, 70000, 'available', 'PN000101'),
('PN0001', 02, 70000, 'available', 'PN000102'),
('PN0002', 01, 9000, 'available', 'PN000201'),
('SK0001', 01, 90000, 'available', 'SK000101'),
('SK0001', 02, 80000, 'available', 'SK000102');

-- --------------------------------------------------------

--
-- Table structure for table `book_publisher`
--

CREATE TABLE `book_publisher` (
  `publisher_id` int(6) UNSIGNED ZEROFILL NOT NULL,
  `publisher_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `book_publisher`
--

INSERT INTO `book_publisher` (`publisher_id`, `publisher_name`) VALUES
(000012, 'NXB Chinh Tri'),
(000015, 'NXB Dep Trai'),
(000020, 'NXB Giai Phong'),
(000011, 'NXB Giao Duc'),
(000013, 'NXB Hai Thanh'),
(000018, 'NXB Khoa Hoc Xa Hoi'),
(000019, 'NXB Kim Dong'),
(000017, 'NXB Quoc Gia'),
(000014, 'NXB Thoi Dai'),
(000016, 'NXB Tuoi Tre');

-- --------------------------------------------------------

--
-- Table structure for table `borrowing_detail`
--

CREATE TABLE `borrowing_detail` (
  `borrowing_id` int(6) UNSIGNED ZEROFILL NOT NULL,
  `copy_id` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `return_date` date DEFAULT NULL,
  `final` int(9) DEFAULT '0',
  `status` set('pending','borrowing','returned','cancelled') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'pending'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `borrowing_detail`
--

INSERT INTO `borrowing_detail` (`borrowing_id`, `copy_id`, `return_date`, `final`, `status`) VALUES
(000011, 'SK000101', NULL, 0, 'cancelled'),
(000012, 'KH000101', NULL, 0, 'cancelled'),
(000012, 'SK000102', NULL, 0, 'cancelled'),
(000016, 'PN000101', NULL, 0, 'cancelled'),
(000017, 'CT000101', NULL, 0, 'cancelled'),
(000017, 'CT000201', '2016-10-28', 0, 'returned'),
(000017, 'GT000101', '2016-10-28', 0, 'returned'),
(000017, 'KH000101', '2016-10-28', 0, 'returned'),
(000018, 'GT000101', '2016-10-28', 0, 'returned'),
(000021, 'GT000102', NULL, 0, 'cancelled'),
(000021, 'PN000101', '2016-10-28', 0, 'returned'),
(000022, 'GT000101', '2016-10-29', 0, 'returned'),
(000023, 'GT000102', '2016-10-29', 0, 'returned'),
(000023, 'PN000101', '2016-10-29', 0, 'returned'),
(000024, 'GT000103', NULL, 0, 'cancelled'),
(000025, 'CT000201', '2016-10-29', 0, 'returned'),
(000027, 'SK000101', '2016-10-29', 15000, 'returned'),
(000030, 'CT000201', '2016-10-29', 15000, 'returned'),
(000030, 'GT000101', NULL, 0, 'cancelled'),
(000030, 'PN000101', NULL, 0, 'cancelled'),
(000031, 'CT000101', '2016-10-29', 15000, 'returned'),
(000031, 'CT000201', NULL, 0, 'pending');

-- --------------------------------------------------------

--
-- Table structure for table `borrowing_general`
--

CREATE TABLE `borrowing_general` (
  `borrowing_id` int(6) UNSIGNED ZEROFILL NOT NULL,
  `card_id` int(6) UNSIGNED ZEROFILL NOT NULL,
  `register_date` date NOT NULL,
  `lent_date` date DEFAULT NULL,
  `expected_return_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `borrowing_general`
--

INSERT INTO `borrowing_general` (`borrowing_id`, `card_id`, `register_date`, `lent_date`, `expected_return_date`) VALUES
(000002, 000001, '2016-10-27', '2016-10-27', '2016-11-10'),
(000010, 000002, '2016-10-27', NULL, NULL),
(000011, 000002, '2016-10-27', NULL, NULL),
(000012, 000002, '2016-10-10', '2016-10-11', '2016-10-18'),
(000013, 000002, '2016-10-27', NULL, NULL),
(000014, 000002, '2016-10-27', NULL, NULL),
(000015, 000002, '2016-10-27', NULL, NULL),
(000016, 000002, '2016-10-27', '2016-10-27', '2016-11-10'),
(000017, 000002, '2016-10-27', '2016-10-28', '2016-11-11'),
(000018, 000002, '2016-10-28', '2016-10-28', '2016-11-11'),
(000019, 000002, '2016-10-28', NULL, NULL),
(000020, 000002, '2016-10-28', NULL, NULL),
(000021, 000002, '2016-10-28', '2016-10-28', '2016-11-11'),
(000022, 000002, '2016-10-28', '2016-10-28', '2016-11-11'),
(000023, 000002, '2016-10-28', '2016-10-28', '2016-11-11'),
(000024, 000002, '2016-10-28', NULL, NULL),
(000025, 000002, '2016-10-28', '2016-10-29', '2016-11-12'),
(000026, 000002, '2016-10-28', NULL, NULL),
(000027, 000002, '2016-10-28', '2016-10-29', '2016-11-12'),
(000028, 000002, '2016-10-29', NULL, NULL),
(000029, 000002, '2016-10-29', NULL, NULL),
(000030, 000002, '2016-10-29', '2016-10-29', '2016-11-12'),
(000031, 000002, '2016-10-29', '2016-10-29', '2016-11-12');

-- --------------------------------------------------------

--
-- Table structure for table `card`
--

CREATE TABLE `card` (
  `card_id` int(6) UNSIGNED ZEROFILL NOT NULL,
  `activate_code` int(9) UNSIGNED ZEROFILL NOT NULL,
  `expired_date` date DEFAULT NULL,
  `user_id` int(6) UNSIGNED ZEROFILL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `card`
--

INSERT INTO `card` (`card_id`, `activate_code`, `expired_date`, `user_id`) VALUES
(000001, 123654789, '2016-10-24', 000004),
(000002, 123654780, '2016-11-16', 000006),
(865476, 987654321, '2017-02-23', 000010),
(865477, 123654788, '2016-11-24', 000010),
(865478, 456789320, '2016-11-16', 000008);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(6) UNSIGNED ZEROFILL NOT NULL,
  `username` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `user_full_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `contact` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` enum('f','m') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'f',
  `student_id` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `student_course` varchar(9) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` enum('activated','inactivated') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'inactivated',
  `role` enum('borrower','librarian','admin') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'borrower',
  `facebook_id` int(6) UNSIGNED ZEROFILL DEFAULT NULL,
  `google_id` int(6) UNSIGNED ZEROFILL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `username`, `password`, `email`, `user_full_name`, `contact`, `gender`, `student_id`, `student_course`, `status`, `role`, `facebook_id`, `google_id`) VALUES
(000004, 'user1', 'User1', '', 'User 1', '0166860116', 'f', NULL, NULL, 'activated', 'borrower', NULL, NULL),
(000005, 'admin', 'Admin', '', 'Admin', NULL, 'f', NULL, NULL, 'activated', 'admin', NULL, NULL),
(000006, 'user2', 'User2', '', 'User2', '0166860116', 'm', NULL, NULL, 'activated', 'borrower', NULL, NULL),
(000007, 'abc', 'abc', '', 'abc', 'abc', 'm', NULL, '', 'inactivated', 'borrower', NULL, NULL),
(000008, 'nguyenthuhang', 'Nguyenthuhang95', '', 'Nguyen Thu Hang', '01668601169', 'f', '201334', '', 'activated', 'borrower', NULL, NULL),
(000009, 'librarian', 'Librarian', '', 'lib', NULL, 'f', NULL, NULL, 'activated', 'librarian', NULL, NULL),
(000010, 'ace', 'ace', 'ace', 'ace', '123456789', 'm', NULL, '', 'activated', 'borrower', NULL, NULL),
(000011, 'xyz', 'xyz', 'xyz', 'xyz', '123456789', 'm', NULL, '', 'activated', 'borrower', NULL, NULL),
(000012, 'lkm', 'lkm', 'lkm', 'lkm', '1234569878', 'm', NULL, '', 'inactivated', 'borrower', NULL, NULL),
(000013, 'user3', 'User3', 'user3@gmail.om', 'user3', '1234569871', 'm', '20133469', '2013-2017', 'inactivated', 'borrower', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `book`
--
ALTER TABLE `book`
  ADD PRIMARY KEY (`book_id`),
  ADD UNIQUE KEY `isbn` (`isbn`),
  ADD KEY `publisher_id` (`publisher_id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `book_category`
--
ALTER TABLE `book_category`
  ADD PRIMARY KEY (`category_id`),
  ADD UNIQUE KEY `category_name` (`category_name`);

--
-- Indexes for table `book_copy`
--
ALTER TABLE `book_copy`
  ADD PRIMARY KEY (`book_id`,`sequence_number`),
  ADD KEY `copy_id1` (`copy_id`),
  ADD KEY `copy_id` (`copy_id`);

--
-- Indexes for table `book_publisher`
--
ALTER TABLE `book_publisher`
  ADD PRIMARY KEY (`publisher_id`),
  ADD UNIQUE KEY `publisher_name` (`publisher_name`);

--
-- Indexes for table `borrowing_detail`
--
ALTER TABLE `borrowing_detail`
  ADD PRIMARY KEY (`borrowing_id`,`copy_id`),
  ADD KEY `copy_id` (`copy_id`);

--
-- Indexes for table `borrowing_general`
--
ALTER TABLE `borrowing_general`
  ADD PRIMARY KEY (`borrowing_id`),
  ADD KEY `card_id` (`card_id`);

--
-- Indexes for table `card`
--
ALTER TABLE `card`
  ADD PRIMARY KEY (`card_id`),
  ADD UNIQUE KEY `activate_code` (`activate_code`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `student_id` (`student_id`),
  ADD UNIQUE KEY `facebook_id` (`facebook_id`),
  ADD UNIQUE KEY `google_id` (`google_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `book_publisher`
--
ALTER TABLE `book_publisher`
  MODIFY `publisher_id` int(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `borrowing_general`
--
ALTER TABLE `borrowing_general`
  MODIFY `borrowing_id` int(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `card`
--
ALTER TABLE `card`
  MODIFY `card_id` int(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=865479;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `book`
--
ALTER TABLE `book`
  ADD CONSTRAINT `book_ibfk_1` FOREIGN KEY (`publisher_id`) REFERENCES `book_publisher` (`publisher_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `book_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `book_category` (`category_id`) ON UPDATE CASCADE;

--
-- Constraints for table `book_copy`
--
ALTER TABLE `book_copy`
  ADD CONSTRAINT `book_copy_ibfk_1` FOREIGN KEY (`book_id`) REFERENCES `book` (`book_id`) ON UPDATE CASCADE;

--
-- Constraints for table `borrowing_detail`
--
ALTER TABLE `borrowing_detail`
  ADD CONSTRAINT `borrowing_detail_ibfk_1` FOREIGN KEY (`borrowing_id`) REFERENCES `borrowing_general` (`borrowing_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `borrowing_detail_ibfk_2` FOREIGN KEY (`copy_id`) REFERENCES `book_copy` (`copy_id`) ON UPDATE CASCADE;

--
-- Constraints for table `borrowing_general`
--
ALTER TABLE `borrowing_general`
  ADD CONSTRAINT `borrowing_general_ibfk_1` FOREIGN KEY (`card_id`) REFERENCES `card` (`card_id`) ON UPDATE CASCADE;

--
-- Constraints for table `card`
--
ALTER TABLE `card`
  ADD CONSTRAINT `card_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
