README

This project hold all the document of the Library System Software development, including source code, requirement specification document, installing instruction document, design interface, design structure ...

In each folder there is a readme.txt file which cover the content of the folder, credit of each member and instruction to use.

Library System Software is a system which can be used by both librarian and borrower, librarian can manage books, check borrower information, confirm borrow request... while borrower can use it to check book information, request to borrow books...

This is a school's subject's project so it still have many mistakes, but I hope you will find some useful skill while programming with Java.

If you have any comment or discussion, please contact teamleader or send mail to following address:

Leader: Hoang Hai Thanh
Member: Mai Xuan Toan